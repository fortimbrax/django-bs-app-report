# -*- coding: utf-8 -*-

from django import forms
from django.db import models
from django.forms import ModelForm
from models import relatorios, totalization_fields
import re

from django import forms
from django.utils.safestring import mark_safe


from django.forms.models import inlineformset_factory



class CheckboxSelectMultipleBr(forms.CheckboxSelectMultiple):
    def render(self, *args, **kwargs): 
        output = super(CheckboxSelectMultipleBr, self).render(*args,**kwargs) 
        return mark_safe(output.replace(u'<ul>', u'').replace(u'</ul>', u'').replace(u'<li>', u'<br>').replace(u'</li>', u''))


class RelatoriosForm(ModelForm):
    form_name = 'relatorios_form'

    # Inicializa todos os campos como hidden field.
    # No __init__ define o widget de quem estah sendo chamadado.
    filter_fields = forms.MultipleChoiceField(required=False, widget=CheckboxSelectMultipleBr())
    group_fields = forms.MultipleChoiceField(required=False, widget=CheckboxSelectMultipleBr())
    config_chart_fields = forms.MultipleChoiceField(required=False, widget=CheckboxSelectMultipleBr())
    select_fields = forms.MultipleChoiceField(required=True, widget=CheckboxSelectMultipleBr())


    """
    Recebe o ContentType e seta o widget dos respectivos checkbox fields. 
    """
    def __init__(self, ct, choices_tuple, *args, **kwargs):
        super(RelatoriosForm, self).__init__(*args, **kwargs) 
        try:
            self.fields['filter_fields'].choices = choices_tuple
            self.fields['group_fields'].choices = choices_tuple
            self.fields['config_chart_fields'].choices = choices_tuple
            self.fields['select_fields'].choices = choices_tuple

            #self.fields['select_fields'].required = True            
            self.fields['title'].required = True            

        except Exception, e:
            print e
            pass

    class Meta:
        model = relatorios
        fields = ['title', 'sessao', 'select_fields', 'filter_fields','group_fields','config_chart_fields']        





# Esse metodo eh bem complicado. 
# Ver como isso estah implementado na view.
# http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset
class TotalizationFieldsForm(ModelForm):
    totalization_field = forms.ChoiceField(choices=(), widget=forms.Select())

    def __init__(self, choices_tuple, *args, **kwargs):
        super(TotalizationFieldsForm, self).__init__(*args, **kwargs)


        self.fields['totalization_field'].choices = choices_tuple

    class Meta:
        model = totalization_fields

