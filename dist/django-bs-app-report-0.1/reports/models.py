# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Q
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType


from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^relatorios\.models\.ListField"])

import ast

#http://brunorocha.org/python/django/django-listfield-e-separetedvaluesfield.html
class ListField(models.TextField):
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)


# Store the fields of allowed models to generate reports.
class ReportsFields(models.Model):
    #model = 
    content_type = models.ForeignKey(ContentType, related_name='+', editable=False)
    internal_type = models.CharField(max_length=100, verbose_name=u'Field internal type', help_text=u'')
    field_dict = models.CharField(max_length=5000, verbose_name=u'Field Dictionary', help_text=u'')


# Create your models here.
class relatorios(models.Model):
    ctype = models.ForeignKey(ContentType, related_name='+', editable=False)
    title = models.CharField(max_length=100, verbose_name=u'Título do relatório', help_text=u'')
    created_by = models.ForeignKey(User, related_name='+', verbose_name=u'Criado por', editable=False, )

    select_fields = models.ManyToManyField(ReportsFields, verbose_name=u'Campos do relatório', related_name='select_fields')
    group_fields = models.ManyToManyField(ReportsFields, verbose_name=u'Campos agrupáveis', related_name='group_fields')
    filter_fields = models.ManyToManyField(ReportsFields, verbose_name=u'Campos filtraveis', related_name='filter_fields', blank=True, null=True)
    config_chart_fields = models.ManyToManyField(ReportsFields, verbose_name=u'Campos do grafico', related_name='config_chart_fields', blank=True, null=True)


    # Como pegar FK com base nos apps. Via ContentType:
    #http://stackoverflow.com/questions/6335986/how-can-i-restrict-djangos-genericforeignkey-to-a-list-of-models
    limit_apps = settings.LIMIT_APPS

    limit = models.Q()
    for la in limit_apps:
        limit = limit | models.Q( app_label=la[0], model=la[1] )

    
    sessao = models.ForeignKey(ContentType,verbose_name='Sessão',limit_choices_to=limit,null=True,blank=True,)
    object_id = models.PositiveIntegerField(verbose_name='related object',null=True,)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__( self ):
        return '%s' % ( self.title )

    def get_absolute_url(self):
        return reverse('relatorios_detail', kwargs={'pk': self.pk})
    
    class Meta:
        verbose_name = "Relatorio"
        verbose_name_plural = "Relatorios"

    def meta(self):
        return self._meta        

    def get_fields(self):
        # Definir os campos que nao serao mostrados no DetailView
        # Deve ser o atributo name, do field do model
        exclui_campo_da_view = ("")
        list_of_tuples = []
        for field in relatorios._meta.fields:
            if not field.name in exclui_campo_da_view:
                # Esse nao consegue retornar o valor de FK
                #list_of_tuples.append((field.verbose_name, field.value_to_string(self)))
                list_of_tuples.append((field.verbose_name, getattr(self, field.name)))
        # Captura os valores dos campos relacionados m2m.
        #http://stackoverflow.com/questions/8474013/loop-in-manytomany-fields-in-django-model
        for field in relatorios._meta.many_to_many:
                if not field.name in exclui_campo_da_view:
                    m2m_field = getattr(self, field.name)   
                    concatenados = ', '.join([str(i) for i in m2m_field.all()])
                    list_of_tuples.append((field.verbose_name, concatenados))
        return list_of_tuples  



# Status de pedido que se aplicam ao plano Simples
GRP_OR_RPT_CHOICES = (
    ('grp', u'Grupo'),
    ('rpt', u'Relatório'),
)
OPER = (
    ('sum_column', u'Soma'),
    ('avg_column', u'Média'),
    ('count_column', u'Contagem'),    
)

class totalization_fields( models.Model):
    report = models.ForeignKey(relatorios, related_name='+', verbose_name=u'Relatório', editable=False, )

    totalization_field = models.CharField(max_length=100, verbose_name=u'Coluna', help_text=u'')
    group_or_report = models.CharField(
        'group_or_report',
        choices=GRP_OR_RPT_CHOICES,
        max_length=18,
    )
    operation = models.CharField(
        'operation',
        choices=OPER,
        max_length=18,
    )    