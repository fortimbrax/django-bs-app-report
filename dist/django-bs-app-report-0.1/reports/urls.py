# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from views import ListView_relatorios, CreateView_relatorios, DetailView_relatorios, UpdateView_relatorios, DeleteView_relatorios, ForReportJson
from django.conf.urls import patterns, include, url

import models


try:
    from django.conf.urls import patterns, url
except ImportError:
    from django.conf.urls.defaults import *



from views import report, report_list


urlpatterns = patterns('',


    url( r'^add/$', 
        (CreateView_relatorios.as_view( model=models.relatorios)),
        {'acao': 'Criar'}, name = 'relatorios_create'),

    url( r'^get_element_for_report/(?P<pk>\d+)$', 
        (ForReportJson.as_view( model=models.relatorios)), name = 'for_report'),


    url( r'^(?P<pk>\d+)$', (DetailView_relatorios.as_view( model=models.relatorios )),
        {'media_saida': 'web',
        'reverso_update': 'relatorios_update',
        'reverso_delete': 'relatorios_delete',
        'reverso_list': 'relatorios_list'}, name = 'relatorios_detail'),

    url( r'^update/(?P<pk>\d+)$', (UpdateView_relatorios.as_view( model=models.relatorios, success_url = reverse_lazy('relatorios_list'))),
        {'media_saida': 'web',
        'reverso_update': 'relatorios_update',
        'reverso_delete': 'relatorios_delete',
        'reverso_list': 'relatorios_list',
        'acao': 'Editar', 'reverso_detail': 'relatorios_detail'}, name = 'relatorios_update'),


    url( r'^delete/(?P<pk>\d+)$', DeleteView_relatorios.as_view( model=models.relatorios, success_url = reverse_lazy('relatorios_list')),
        {'media_saida': 'web',
        'reverso_update': 'relatorios_update',
        'reverso_delete': 'relatorios_delete',
        'reverso_detail': 'relatorios_detail',
        'reverso_list': 'relatorios_list'}, name = 'relatorios_delete'),


    # URLS sem definicao de objeto manda para listagem
    url( r'^list/$', (ListView_relatorios.as_view( model=models.relatorios, paginate_by = 10 )),
        name = 'relatorios_list'),   

   
    url(r'^$', report_list, name='model_report_list'),
    url(r'^(?P<slug>[\w-]+)/$', report, name='model_report_view'),    
)


