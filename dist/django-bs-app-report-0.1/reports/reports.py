from models import relatorios
from report import reports, ReportAdmin

#from utils import (usd_format, avg_column, sum_column, count_column)


# def created_by__username_label(report, field):
#     return _("[Browser] Name")

class AnyModelReport(ReportAdmin):

    #proposta_14.__unicode__()

    # title = ('Proposta')
    # model = proposta_14
    # fields = [
    # 'created_by.__unicode__',
    #     'titulo',
    #    # 'descricao_necessidade',
    #     'data',
    #     'data__year',        
    #     'data__month',        

    #     'data_de_fechamento',
    #     'funilstatus',
    #     'tipo_de_venda.__unicode__',
    #     'empresa_10_field.__unicode__',
    #     'contato_12_field.__unicode__',
    #     'valor_da_proposta',
    # ]
    #list_filter = ('created_by', 'empresa_10_field', 'funilstatus')


#
#
############## Pegando os dados lah no comeco desse arquivo.
############## Precisa recuperar no get quem estah sendo chamado,
############## filtrar de acordo e retornar os dados corretos.
#


    # list_filter = filter_fields_tuple
    # list_order_by = ('titulo',)
    # list_group_by = ('created_by', 'empresa_10_field', 'funilstatus', 'valor_da_proposta','data__year', 'data__month')
    # list_serie_fields = ('created_by', 'empresa_10_field', 'funilstatus', 'valor_da_proposta','data__year', 'data__month')

    #type = 'report'
    type = 'chart'


    # override_field_labels = {
    #     'created_by__username': created_by__username_label,
    # }

    # group_totals = {
    #     'titulo': count_column,
    #     'valor_da_proposta': sum_column,
    # }

    # report_totals = {
    #     'titulo': count_column,
    #     'valor_da_proposta': sum_column,
    # }
    chart_types = ('pie', 'column', 'line')


reports.register('report', AnyModelReport)

