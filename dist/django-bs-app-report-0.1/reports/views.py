# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.conf import settings
from django.http import Http404

from report import reports
import json

def report_list(request):
    """
    This view render all reports registered
    """
    context = {
        'report_list': reports.get_reports()
    }
    return render_to_response('model_report/report_list.html', context, context_instance=RequestContext(request))


def report(request, slug):

    print "abababababababab" 
    """
    This view render one report

    Keywords arguments:
    
    slug -- slug of the report
    """
    report_class = reports.get_report(slug)

    # print dir(report_class)
    # print report_class.model

    if not report_class:
        raise Http404
    context = {
        'report_list': reports.get_reports()
    }
    
    report = report_class(request=request)
    return report.render(request, extra_context=context)


# -*- coding: utf-8 -*-


from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect

from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string
from django.shortcuts import render, render_to_response

from django.forms.models import inlineformset_factory
from django.db.models.loading import get_model
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.core.urlresolvers import resolve
from django.utils.functional import curry

from django.template import loader 


from django.views.generic.detail import *
from django.views.generic.list import *
from django.views.generic.edit import *

from django.template import Context
from django import forms

import pickle
import string
import sys
import re
import json

from models import relatorios, ListField, totalization_fields, ReportsFields
from forms import RelatoriosForm,  TotalizationFieldsForm





class ForReportJson( CreateView): 
    template_name = 'relatorios_form_json.html'
    model = relatorios

    def get(self,request, *args, **kwargs):

        if kwargs.get('pk', None):
            try:
                ct = ContentType.objects.get(pk = kwargs['pk'])
                # print ct.model
                
                rfs = ReportsFields.objects.filter(content_type = ct)

                # Monta lista para poder dar append e depois converte em tupla
                # # A tupla contem (name, verbose_name) do field.
                choices_list = []
                for rf in rfs:
                    rf_json = pickle.loads(rf.field_dict)
                    choices_list.append((rf.id, rf_json.verbose_name))
                choices_tuple = tuple(choices_list)

                form_class = RelatoriosForm(ct=ct, choices_tuple=choices_tuple)

                # Insert this null value to not change the non changed fields.
                # Otherwise will gonna have problem here form.has_changed(), on the view.        
                choices_list.insert(0, ('','-----'))

                # inlineformset_factory. Inicializa com choices_tuple
                # http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset
                # Eh buxa!
                ReportFormSet = inlineformset_factory(relatorios, totalization_fields, form=TotalizationFieldsForm)
                ReportFormSet.form = staticmethod(curry(TotalizationFieldsForm, choices_tuple=choices_list))

                data = {'TOTAL_FORMS': '10', 'INITIAL_FORMS': '0', 'MAX_NUM_FORMS': ''}
                tf = ReportFormSet(data, instance=relatorios)
            except Exception, e:
                print e
            
            return render_to_response('relatorios_form_json.html', {'form': form_class, 'fields':choices_tuple, 'ctype':kwargs['pk'], 'totalization_fields':tf})

            #html = render_to_string('relatorios_form_json.html', {'form': form_class, 'fields':choices_tuple, 'ctype':kwargs['pk'], 'totalization_fields':totalization_fields})
            #print html

#
### Mandar para listagem se nao vier o PK
#



class CreateView_relatorios( CreateView ):            
    template_name = 'relatorios_form.html'

    def store_fields(self):
        limit_apps = settings.LIMIT_APPS
        for la in limit_apps:
            ct = ContentType.objects.get(model = la[1])
            elemento = get_model(ct.app_label, ct.model)

            for field in elemento._meta.fields:
                reports_fields = ReportsFields()
                reports_fields.content_type = ct
                reports_fields.internal_type = str(field.get_internal_type())
                #reports_fields.field_dict = str(field.__dict__)
                reports_fields.field_dict = pickle.dumps(field)
                reports_fields.save()

                #print pickle.dumps(field)

                #exit(0)
            #print elemento

    def get(self,request, acao):
        # self.store_fields()
        form_class = RelatoriosForm('', '')
        return self.render_to_response({'acao':acao, 'form':form_class})

    def post(self, request, *args, **kwargs):
        try:
            ct = ContentType.objects.get(pk = request.POST.get("ctype_id"))
            rfs = ReportsFields.objects.filter(content_type = ct)
            
            # Monta lista para poder dar append e depois converte em tupla
            # A tupla contem (name, verbose_name) do field.
            choices_list = []
            date_fields_list = []
            date_group_list = []
            date_filter_list = []
            date_chart_list = []

            for rf in rfs:
                # print field.get_internal_type()
                # field_name = ''
                rf_json = pickle.loads(rf.field_dict)

                choices_list.append((rf.id, rf_json.verbose_name))
            choices_tuple = tuple(choices_list)

            # Vem a maior sujeiro do checkbox select multiple sortable do jquery.
            # Limpa para passar para o campo selected_fields, que eh do tipo lista. 
            selected_fields = []
            clean_string =  request.POST.get('select_fields').split("&")   
            for c in clean_string:
                c = string.replace(c, 'select_fields[]=', '')
                c = string.replace(c, 'select_fields_', '')
                c = string.replace(c, '[]=', '_')
                c = string.replace(c, '(', '')
                c = string.replace(c, ')', '')                   

                c_list = c.split(",")
                #selected_fields = selected_fields + ',' + c_list[0]
                selected_fields.append(c_list[0])


                print selected_fields
                #selected_fields.append(c_list)


            request.POST = request.POST.copy()
            request.POST.setlist( 'select_fields',  selected_fields )
            # This is to avoid this
            # http://stackoverflow.com/questions/618699/a-workaround-for-django-querydict-wrapping-values-in-lists

            form_class = RelatoriosForm(ct=ct, choices_tuple=choices_tuple, data=request.POST)

        except Exception, e:
            print "merda"
            print e
            print sys.exc_traceback.tb_lineno 


        try:

            if form_class.is_valid():
                relatorio_obj = form_class.save(commit=False)

                ReportFormSet = inlineformset_factory(relatorios, totalization_fields)
                
                # ReportFormSet = inlineformset_factory(relatorios, totalization_fields, form=TotalizationFieldsForm)
                # ReportFormSet.form = staticmethod(curry(TotalizationFieldsForm, choices_tuple=choices_tuple))
                # print ReportFormSet(request.POST)

                # print '---------'
                # print date_group_list
                # print date_chart_list
                # print '---------'                

                tf_fs = ReportFormSet(request.POST, instance=relatorio_obj)
                if tf_fs.is_valid():
                    relatorio_obj.created_by = self.request.user
                    relatorio_obj.ctype = ct

                    relatorio_obj.save()
                    tf_fs.save()
                    form_class.save_m2m()
                    return HttpResponseRedirect(reverse('relatorios_list'))
                else:
                    t = loader.get_template('relatorios_form_json.html')
                    c = Context({'form': form_class, 'tf_fs':tf_fs.errors})
                    return HttpResponse(t.render(c))

            else:
                pass
                t = loader.get_template('relatorios_form_json.html')
                c = Context({'form': form_class, 'fields':choices_tuple, 'ctype':ct.id})
                return HttpResponse(t.render(c))

        except Exception, e:
            print "shit"
            print e
            print sys.exc_traceback.tb_lineno 
            print form_class.errors

            t = loader.get_template('relatorios_form_json.html')
            c = Context({'form': form_class, 'fields':choices_tuple, 'ctype':ct.id})
            return HttpResponse(t.render(c))



class UpdateView_relatorios( UpdateView ):
    model = relatorios
    template_name = 'relatorios_update_form.html'

    def get_context_data(self, **kwargs):
        try:
            context = super(UpdateView_relatorios, self).get_context_data(**kwargs)
            obj = self.get_object()
            ct = ContentType.objects.get(pk = obj.ctype_id)

            rfs = ReportsFields.objects.filter(content_type = ct)

            # Monta lista para poder dar append e depois converte em tupla
            # # A tupla contem (name, verbose_name) do field.
            choices_list = []
            for rf in rfs:
                rf_json = pickle.loads(rf.field_dict)
                choices_list.append((rf.id, rf_json.verbose_name))
            choices_tuple = tuple(choices_list)

            print '------------' 
            print choices_tuple
            print '------------' 

            form_class = RelatoriosForm(ct=ct, choices_tuple=choices_tuple, instance=obj)

            # Insert this null value to not change the non changed fields.
            # Otherwise will gonna have problem here form.has_changed(), on the view.        
            choices_list.insert(0, ('','-----'))
            
            # inlineformset_factory. Inicializa com choices_tuple
            # http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset
            # Eh buxa!
            ReportFormSet = inlineformset_factory(relatorios, totalization_fields, form=TotalizationFieldsForm)
            ReportFormSet.form = staticmethod(curry(TotalizationFieldsForm, choices_tuple=choices_list))

            #data = {'TOTAL_FORMS': '10', 'INITIAL_FORMS': '0', 'MAX_NUM_FORMS': ''}
            #tf = ReportFormSet(data, instance=relatorios)

            select_fields = []
            for sf in self.object.select_fields.all():
                rf_json = pickle.loads(sf.field_dict)
                select_fields.append([sf.id, rf_json.verbose_name]) 

            tf = ReportFormSet(instance=obj)
            context.update({
                'form': form_class,
                'ct':ct,
                'fields':choices_tuple, 
                'select_fields': select_fields,
                'tf': tf,
            })
            return context 

        except Exception, e:
            print e
            print sys.exc_traceback.tb_lineno 

    def get_success_url(self):
        obj = self.get_object()
        return reverse('relatorios_list', kwargs={ 'sistema_id': obj.sistema_id})

    def post(self, request, *args, **kwargs):
        try:
            ct = ContentType.objects.get(pk = request.POST.get("ctype_id"))

            rfs = ReportsFields.objects.filter(content_type = ct)

            # Monta lista para poder dar append e depois converte em tupla
            # # A tupla contem (name, verbose_name) do field.
            choices_list = []
            for rf in rfs:
                rf_json = pickle.loads(rf.field_dict)
                choices_list.append((rf.id, rf_json.verbose_name))
            choices_tuple = tuple(choices_list)

            
            # Vem a maior sujeiro do checkbox select multiple sortable do jquery.
            # Limpa para passar para o campo selected_fields, que eh do tipo lista. 
            selected_fields = []
            clean_string =  request.POST.get('select_fields').split("&")   
            for c in clean_string:
                c = string.replace(c, 'select_fields[]=', '')
                c = string.replace(c, 'select_fields_', '')
                c = string.replace(c, '[]=', '_')
                c = string.replace(c, '(', '')
                c = string.replace(c, ')', '')                   

                c_list = c.split(",")
                selected_fields.append(c_list[0])


            request.POST = request.POST.copy()
            request.POST.setlist( 'select_fields',  selected_fields )

            print choices_tuple
            print selected_fields
            print request.POST

#            request.POST['select_fields'] = selected_fields
            #form_class = RelatoriosForm(ct=ct, choices_tuple=choices_tuple, data=request.POST)
            form_class = RelatoriosForm(ct=ct, choices_tuple=choices_tuple, data=request.POST, instance=self.get_object())

        except Exception, e:
            print "merda"
            print e
            print sys.exc_traceback.tb_lineno 

        try:
            if form_class.is_valid():
                relatorio_obj = form_class.save(commit=False)

                ReportFormSet = inlineformset_factory(relatorios, totalization_fields)
                
                # ReportFormSet = inlineformset_factory(relatorios, totalization_fields, form=TotalizationFieldsForm)
                # ReportFormSet.form = staticmethod(curry(TotalizationFieldsForm, choices_tuple=choices_tuple))
                # print ReportFormSet(request.POST)

                tf_fs = ReportFormSet(request.POST, instance=relatorio_obj)
                if tf_fs.is_valid():

                    relatorio_obj.created_by = self.request.user
                    relatorio_obj.ctype = ct

                    relatorio_obj.save()
                    tf_fs.save()
                    form_class.save_m2m()

                    return HttpResponseRedirect(reverse('relatorios_list'))
                else:
                    t = loader.get_template('relatorios_update_form.html')
                    c = Context({'form': form_class, 'tf_fs':tf_fs.errors})
                    return HttpResponse(t.render(c))

            else:
                t = loader.get_template('relatorios_update_form.html')
                c = Context({'form': form_class})
                return HttpResponse(t.render(c))

        except Exception, e:
            print e
            print sys.exc_traceback.tb_lineno 

            t = loader.get_template('relatorios_update_form.html')
            c = Context({'form': form_class, 'fields':choices_tuple, 'ctype':ct.id})
            return HttpResponse(t.render(c))




class DetailView_relatorios( DetailView ):
    template_name = 'reports_detail.html'
    pass
    # def get_context_data(self, **kwargs):
    #         context = super(DetailView_relatorios, self).get_context_data(**kwargs)

    #         sistema = Sistema.objects.get(id=self.kwargs['pk'])
    #         context.update({
    #             'sistema': sistema
    #         })
    #         return context 

class DeleteView_relatorios( DeleteView ):
    template_name = 'relatorios_confirm_delete.html'

    def get_context_data(self, **kwargs):
        context = super(DeleteView_relatorios, self).get_context_data(**kwargs)
        object_name = 'reports'
        object_id = self.object.pk
        reverso_detail = self.kwargs['reverso_detail']
        context.update({
            'reverso_detail': reverso_detail, 'object_name': object_name, 'object_id': object_id
        })
        return context

class ListView_relatorios( ListView ):
    template_name = 'reports_list.html'
    pass


