Download
========
Download the package from here:
https://bitbucket.org/fortimbrax/django-bs-app-report/src/6d701e5490875c5c1add5d24d6b36a0366728cca/dist/django-bs-app-report-0.1.tar.gz?at=master

Install
=======
If you use virtualenv, call your virtualenv first and then:
pip install django-bs-app-report-0.1.tar.gz

urls.py
=======
Configure urls.py as bellow. Autodiscover will work as the same for admin site.
from reports import report
report.autodiscover()

You can choose the url better fit your needs.
app_reports = url(r'^app_reports/', include('reports.urls'))

report.urls has two main urls (/add and /list) where you can access the other urls for the CRUD.

settings.py
===========
Configure settings.py
INSTALLED_APPS = (
	reports,
)

Still on settings.py, create the tupla of tuplas of the apps and regarded models 
you want to create reports for.
LIMIT_APPS=(('app1','model1'),('app1', 'model2'),
            ('app3','model1'),('app_n', 'model_n'))

The app name shall be written exactly the same as in the column app_label of the table django_content_type.
The model name shall be written exactly the same as in the column model of the table django_content_type.

Migrate (TODO)
=========
./manage.py migrate -------> I need to test this step


Init (TODO)
====
To initialize DB with fields of the tables that will be queried, need to uncomment this on views.py:
self.store_fields
After uncomment, run once only the url /add. This will charge database. After that, comment again, otherwise it will duplicate entries.



base.html
=========
django-bs-app-report relies on twitter bootstrap to organize the layout. If you use other base than base.html,
go to reports/template/reports_base.html and change the template you need to use. This is the only point that you
need to change in order to define your base.html.
